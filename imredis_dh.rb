#!/usr/bin/env ruby
# DH CLI Tool


require 'bundler/setup'
require 'im-database'
require 'im-redis'
require 'logger'
require 'pry'

@log = $log || Logger.new(STDOUT)

# DB Configuration
config   = File.read(ENV['BLINK_CONFIG'] + '/private/db.im')
db       = JSON.parse(config, :symbolize_names => true)
IMDatabase::Database.setup db, :log_level => :debug

config   = File.read(ENV['BLINK_CONFIG'] + '/region.im')
region   = JSON.parse(config, :symbolize_names => true)

config = File.read(ENV['BLINK_CONFIG'] + "/private/environment.yml")
env    = YAML.load(config)
@client = IMRedis::Client.new config: env['redis'], tier: region[:dns]
@redis = @client.redis


# Add a DH to redis list to block access
def add_dh(account_id)
  acc = IMDatabase::Account.get(account_id)
  puts "Account #{account_id} not found" unless acc

  # Pull email and all auth tokens for this account
  email   = acc.users.last.email
  tokens  = acc.tokens.all.map(&:auth)


  # If the key is already on the DH list, skip
  if @client.dh_exists?(email)
    puts "Account #{account_id} @ #{email} already exists in the DH list"
    return false
  end


  # Confirmation
  puts "Adding #{email} to dh list with tokens #{tokens}"
  print "Are you sure you want to proceed [Y/n]: "
  proceed = STDIN.gets.strip.downcase

  return if ['n', 'no'].include?(proceed)

  # Add email and tokens to individual lists, then populate
  # hash set of email -> tokens
  @client.dh_add_email(email)
  @client.dh_add_tokens(tokens)
  @client.dh_add_pairs(email, tokens)


  # Verify email and token additions
  puts "Added #{email} and all tokens to dh_emails and dh_tokens"
  puts "#{email} - #{@client.dh_email?(email)}"

  tokens.each {|x| puts "#{x} - #{@client.dh_token?(x)}"}
  puts "Added #{email} and tokens to dh_pairs"

  puts @client.dh_get(email)
  true
end # add_dh


# Remove a DH from redis list to allow access
def remove_dh(account_id)
  acc = IMDatabase::Account.get(account_id)
  puts "Account #{account_id} not found" unless acc

  email = acc.users.last.email


  # If key isn't in pair, then skip
  unless @client.dh_exists?(email)
    puts "Account #{account_id} @ #{email} does not exists in the DH list"
    return false
  end


  # Confirmation
  puts "Removing #{email} from dh list"
  print "Are you sure you want to proceed [Y/n]: "
  proceed = STDIN.gets.strip.downcase

  return if ['n', 'no'].include?(proceed)

  # Using email->tokens hash entry, remove email and tokens from
  # individual lists
  email, tokens = @client.dh_remove(email)


  # Verify email and token removal
  puts "Removed #{email} and all tokens to dh_emails and dh_tokens"
  puts "#{email} - #{@client.dh_email?(email)}"

  tokens.each {|x| puts "#{x} - #{@client.dh_token?(x)}"}
  puts "Removed #{email} and tokens to dh_pairs"

  puts @client.dh_get(email)
  true
end # remove_dh


def set_sm_dh_config(options, serial: nil)
  serial ||= ""
  raise "must pass hash for config" unless options.is_a? Hash

  config = @client.sm_go_away serial: serial
  config.merge!({
    :status           => options[:status] || config[:status],
    :action           => options[:action] || config[:action],
    :host             => options[:host] || config[:host],
    :port             => options[:port] || config[:port],
    :connect_time     => options[:connect_time] || config[:connect_time],
    :config           => options[:config] || config[:config]
  })

  # make sure that the config structure exists
  raise "config does not meet config requirements [#{@client.default_sm_go_away.keys}]" unless (@client.default_sm_go_away.keys - config.keys).empty?

  key = serial.empty? ? IMRedis::Client::DH_SM_CONFIG_KEY : IMRedis::Client::DH_SM_CUSTOM_CONFIG_KEY + serial

  @client.redis.set(key, JSON.generate(config))
  ap JSON.parse(@client.redis.get(key))
end # set_config



def clear_sm_dh_config(serial: nil)
  serial ||= ""
  key = IMRedis::Client::DH_SM_CUSTOM_CONFIG_KEY + serial


  if key == IMRedis::Client::DH_SM_CONFIG_KEY
    puts "You can't clear the default config!!! [ #{key} ]"
    return
  end

  @client.redis.del(key)
  true
end # clear_sm_dh_config


binding.pry
