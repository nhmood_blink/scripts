#!/bin/bash


csv_path=$1
container=blink-sm-health
image_tag=201609291336
image_tag=20160929191530
image_tag=20160929191950
image_tag=latest

exists=$(docker ps -q -a --filter name=${container} | wc -l)
if [ ${exists} != "0" ]; then
    docker rm -v -f ${container}
fi

docker run \
    -it \
    --name ${container} \
    -v /home/ec2-user/.ssh/id_rsa:/home/eugene/.ssh/id_rsa \
    -v /home/ec2-user/utility:/home/eugene/utility \
    -v /home/ec2-user/system:/home/eugene/system \
    -v /home/ec2-user/config:/home/eugene/config \
    -v /home/ec2-user/init_container.sh:/home/eugene/init_container.sh \
    -v /home/ec2-user/ssh_config:/home/eugene/.ssh/config \
    -v /home/ec2-user/sm_health/:/home/eugene/sm_health \
    blink-process:${image_tag} \
    /bin/bash -l -c "cd /home/eugene/system/process && /home/eugene/sm_health/sm_health.rb"
