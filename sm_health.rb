#!/usr/bin/env ruby
# SM Health Redis Setter


require 'bundler/setup'
require 'json'
require 'yaml'
require 'redis'
require 'logger'
require 'pry'
require 'awesome_print'

@log = $log || Logger.new(STDOUT)

config = File.read(ENV['BLINK_CONFIG'] + "/private/environment.yml")
env    = YAML.load(config)
@redis = Redis.new(:host => env['redis']['host'], :port => env['redis']['port'], :timeout => env['redis']['timeout']) 
@region = env['tier']['name']



SM_CONFIG = {
  :status   => :ok,
  :rate     => 0
}

SM_STATUS_OK = {
  :status           => "ok",
  :action           => "cs",
  :host             => "cs.#{@region}.immedia-semi.com",
  :port             => 443,
  :connect_time     => 5,
  :config           => {
    :ack_duration     => 15
  }
}


SM_STATUS_BAD = {
  :status           => "bad",
  :action           => "rest",
  :host             => "rest.#{@region}.immedia-semi.com",
  :port             => 443,
  :connect_time     => 5,
  :config           => { }
}


def set_config(type, options)
  raise "only :ok and :bad configs are acceptable at the current time" unless [:ok, :bad].include?(type)
  raise "must pass hash for config" unless options.is_a? Hash

  config = type == :ok ? SM_STATUS_OK.dup : SM_STATUS_BAD.dup
  config.merge!({
    :host             => options[:host] || config[:host],
    :connect_time     => options[:connect_time] || config[:connect_time],
    :config           => options[:config] || config[:config]
  })

  # make sure that the config structure exists
  raise "config does not meet config requirements [#{SM_STATUS_OK.keys}]" unless (SM_STATUS_OK.keys - config.keys).empty?

  @redis.set("rest:sm_status:#{type}", JSON.generate(config))
  ap JSON.parse(@redis.get("rest:sm_status:#{type}"))
end # set_config


def set_default_configs
  puts "setting defaults"
  @redis.set("rest:sm_status:ok",   JSON.generate(SM_STATUS_OK))
  ap JSON.parse(@redis.get("rest:sm_status:ok"))
  @redis.set("rest:sm_status:bad",  JSON.generate(SM_STATUS_BAD))
  ap JSON.parse(@redis.get("rest:sm_status:bad"))
end # set_default_configs


def set_status(status, rate: 5)
  raise "Invalid status type: #{status}" unless [:ok, :bad, :bad_sm, :transition, :transition_sm].include?( status )
  raise "Invalid rate: #{rate}" unless rate.is_a? Numeric

  set_default_configs unless @redis.exists("rest:sm_status:ok") && @redis.exists("rest:sm_status:bad")

  config = {
    :status => status,
    :rate   => rate
  }

  config_key = "rest:sm_status:config"
  puts "Setting #{config_key}"

  @redis.hmset(config_key, config.to_a.flatten)
  ap @redis.hgetall("rest:sm_status:config")
end # set_status


binding.pry
