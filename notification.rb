#!/usr/bin/env ruby
# Notification Sender
#
# nhmood @ immedia
# may 4th, 2017


require 'bundler/setup'
require 'im-database'
require 'yaml'
require 'grocer'


path  = File.expand_path(ENV['BLINK_CONFIG'])
env   = YAML.load(File.read(path + '/private/environment.yml'))
db    = JSON.parse(File.read(path + '/private/db.im'), :symbolize_names => true)

@db   = IMDatabase::Database.setup db 
@tier = env["s3"]["s3_group"]


email = ARGV[0]

user = IMDatabase::User.first(:email => email)
client = user.account.clients.reject {|x| x.notification_key.empty? }.last
notification = user.notifications.last

abort("NO NOTIFICATION KEY FOUND FOR #{email}") if client.nil?
abort("ACCOUNT ALREADY VERIFIED FOR #{email}") if user.verified

account_id = user.account_id
uid = SecureRandom.urlsafe_base64(12)
payload = {
  :unique_id        => uid,
  :account_id       => client.account_id,
  :tier             => @tier,
  :category         => :health,
  :network          => nil,
  :network_name     => nil,
  :sync_module      => nil,
  :camera           => nil,
  :camera_name      => nil,
  :event_type       => :email_verification,
  :event_status     => true,
  :return_receipt   => "true",
  :help_url         => "http://google.com",

}


alert_text = {
  :body => "Your email address #{user.email} hasn't been verified yet!",
  "loc-key" => "Your email address %1$@ hasn't been verified yet!",
  "loc-args" => [ user.email ]
}


obj = {
  :msg                => "TEST",
  :notification_key   => client.notification_key,
  :notification_type  => notification,
  :notification_name  => client.name.strip.length.zero? ? "Apple" : client.name,
  :event              => 0,
  :sync_module_id     => nil,
  :alert_text         => alert_text,
  :new_client         => false,
  :client_type        => client.client_type,
  :alert_payload      => payload
}



apns_path = (ENV['BLINK_CONFIG']) + "/private/" + File.basename(env["notification"]["cert_file"])
apns_gateway = env["notification"]["APNS_gateway"]

ap apns_path
ap apns_gateway

# Return the APNS pusher object
pusher = Grocer.pusher(
  certificate: apns_path,                # required
  passphrase:  "",                       # optional
  gateway:     apns_gateway,            # optional; See note below.
  port:        2195,                     # optional
  retries:     3                         # optional
)


send = {
  :device_token => client.notification_key,
  :alert        => obj[:alert_text],
  :custom       => obj[:alert_payload],
  :sound        => "default",
  :expiry       => Time.now + env["notification"]["APNS_retry_secs"]
}


ap send

notification = Grocer::Notification.new(send)

# Return the status of the sender
push = pusher.push(notification)
