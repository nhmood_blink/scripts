#!/usr/bin/env ruby
# DH CLI Tool


require 'bundler/setup'
require 'yaml'
require 'redis'
require 'logger'
require 'pry'

@log = $log || Logger.new(STDOUT)

config = File.read(ENV['BLINK_CONFIG'] + "/private/environment.yml")
env    = YAML.load(config)
@redis = Redis.new(:host => env['redis']['host'], :port => env['redis']['port'], :timeout => env['redis']['timeout']) 



binding.pry
