#!/usr/bin/env ruby
# DH CLI Tool


require 'bundler/setup'
require 'im-database'
require 'redis'
require 'logger'
require 'pry'

@log = $log || Logger.new(STDOUT)

# DB Configuration
config   = File.read(ENV['BLINK_CONFIG'] + '/private/db.im')
db       = JSON.parse(config, :symbolize_names => true)
IMDatabase::Database.setup db, :log_level => :debug


config = File.read(ENV['BLINK_CONFIG'] + "/private/environment.yml")
env    = YAML.load(config)
@redis = Redis.new(:host => env['redis']['host'], :port => env['redis']['port'], :timeout => env['redis']['timeout']) 


# Add a DH to redis list to block access
def add_dh(account_id)
  acc = IMDatabase::Account.get(account_id)
  puts "Account #{account_id} not found" unless acc

  # Pull email and all auth tokens for this account
  email   = acc.users.last.email
  tokens  = acc.tokens.all.map(&:auth)


  # If the key is already on the DH list, skip
  if @redis.hget('dh_pairs', email)
    puts "Account #{account_id} @ #{email} already exists in the DH list" 
    return false
  end

  # Confirmation
  puts "Adding #{email} to dh list with tokens #{tokens}"
  print "Are you sure you want to proceed [Y/n]: "
  proceed = STDIN.gets.strip.downcase

  return if ['n', 'no'].include?(proceed)

  # Add email and tokens to individual lists, then populate
  # hash set of email -> tokens
  @redis.sadd('dh_emails', email)
  @redis.sadd('dh_tokens', tokens)
  @redis.hset('dh_pairs', email, tokens.join(", "))

  # Verify email and token additions
  puts "Added #{email} and all tokens to dh_emails and dh_tokens"
  puts "#{email} - #{@redis.sismember('dh_emails', email)}"

  tokens.each {|x| puts "#{x} - #{@redis.sismember('dh_tokens', x)}"}
  puts "Added #{email} and tokens to dh_pairs"

  puts @redis.hget('dh_pairs', email)
  true
end # add_dh


# Remove a DH from redis list to allow access
def remove_dh(account_id)
  acc = IMDatabase::Account.get(account_id)
  puts "Account #{account_id} not found" unless acc

  email = acc.users.last.email

  # If key isn't in pair, then skip
  unless @redis.hget('dh_pairs', email)
    puts "Account #{account_id} @ #{email} does not exists in the DH list" 
    return false
  end


  # Confirmation
  puts "Removing #{email} from dh list"
  print "Are you sure you want to proceed [Y/n]: "
  proceed = STDIN.gets.strip.downcase

  return if ['n', 'no'].include?(proceed)

  # Using email->tokens hash entry, remove email and tokens from
  # individual lists
  tokens = @redis.hget('dh_pairs', email).split(',').map(&:strip)
  tokens.each {|x| @redis.srem('dh_tokens', x) }
  @redis.srem('dh_emails', email)

  # Delete the email hash entry
  @redis.hdel('dh_pairs', email)


  # Verify email and token removal
  puts "Removed #{email} and all tokens to dh_emails and dh_tokens"
  puts "#{email} - #{@redis.sismember('dh_emails', email)}"

  tokens.each {|x| puts "#{x} - #{@redis.sismember('dh_tokens', x)}"}
  puts "Removed #{email} and tokens to dh_pairs"

  puts @redis.hget('dh_pairs', email).nil?
  true
end # remove_dh



# Check if given email or token is a DH
def is_dh?(member)
  is_dh_email?(member) || is_dh_token?(member)
end # is_dh?


# Check if given email is a DH
def is_dh_email?(email)
  begin
    @redis.sismember('dh_emails', email)
  rescue StandardError => e
    @log.info "Redis DH email call failed, passing through"
    @log.info e.inspect
    return false
  end 
end # is_dh_email?


# Check if given token is a DH
def is_dh_token?(token)
  begin
    @redis.sismember('dh_tokens', token)
  rescue StandardError => e
    @log.info "Redis DH token call failed, passing through"
    @log.info e.inspect
    return false
  end 
end # is_dh_token?

binding.pry
